﻿using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Collections;
using WebdriverClass.Pages;

namespace WebdriverClass
{ 
    class BadandoPageObjectTest : TestBase
    {
        [Test]
        public void WebsiteAccessTest()
        {
            int noOfResults= MainPage.Navigate(Driver)
                .GetResultMainWidget()
                .GetNoOfResults();
            Assert.That(noOfResults > 0);
        }
        [Test, TestCaseSource("MenuData")]
        public void MenuTest(string name, string desc)
        {
            string content = MainPage.Navigate(Driver)
                .GetButtonWidget()
                .ClickButton(name)
                .GetResultButtonWidget()
                .GetContent();
            Console.WriteLine(content);
            Assert.That(content.Contains(desc));
        }
        static IEnumerable MenuData()
        {
            var doc = XElement.Load(System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory) + "\\menus.xml");
            return
                from vars in doc.Descendants("button")
                let name = vars.Attribute("name").Value
                let desc = vars.Attribute("desc").Value
                select new object[] { name, desc };
        }
    }
}
