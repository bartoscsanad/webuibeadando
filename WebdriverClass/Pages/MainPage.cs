﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebdriverClass.Widgets;

namespace WebdriverClass.Pages
{
    class MainPage : BasePage
    {
        public MainPage(IWebDriver driver) : base(driver)
        {
        }
        public static MainPage Navigate(IWebDriver webDriver)
        {
            // Navigate to "https://neptun.uni-obuda.hu/"
            webDriver.Navigate().GoToUrl("https://neptun.uni-obuda.hu/");
            return new MainPage(webDriver);
        }
        public ButtonWidget GetButtonWidget()
        {
            return new ButtonWidget(Driver);
        }
        public ResultButtonWidget GetResultButtonWidget()
        {
            return new ResultButtonWidget(Driver);
        }
        public ResultMainWidget GetResultMainWidget()
        {
            return new ResultMainWidget(Driver);
        }
    }
}
