﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebdriverClass.Widgets
{
    class ResultButtonWidget : BasePage
    {
        public ResultButtonWidget(IWebDriver driver) : base(driver)
        {
        }
        public string GetContent()
        {
            string content = Driver.FindElement(By.ClassName("left-corner")).Text;
            return content;
        }
    }
}
