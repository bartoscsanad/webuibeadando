﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebdriverClass.Widgets
{
    class ResultMainWidget : BasePage
    {
        public ResultMainWidget(IWebDriver driver) : base(driver)
        {
        }
        private IWebElement menu => Driver.FindElement(By.Id("block-menu-64"));
        private List<IWebElement> Lines => menu.FindElements(By.XPath(".//ul/li/a")).ToList();
        public int GetNoOfResults()
        {
            return Lines.Count;
        }
    }
}
