﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebdriverClass.Pages;

namespace WebdriverClass.Widgets
{
    class ButtonWidget : BasePage
    {
        public ButtonWidget(IWebDriver driver) : base(driver)
        {
        }
        public MainPage ClickButton(string buttonClass)
        {
            Driver.FindElement(By.CssSelector(buttonClass)).Click();

            return new MainPage(Driver);
        }
    }
}
